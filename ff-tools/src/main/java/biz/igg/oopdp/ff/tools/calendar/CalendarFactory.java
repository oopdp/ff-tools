/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools.calendar;

/**
 *
 * @author salvix
 */
public class CalendarFactory {
	
	private AbstractCalendar c;
	
	public CalendarFactory(String factory, String input)
	{
		if(factory.equals("MyFxBook")){
			c = new MyFxBook(input);
		}
	}
	
	public AbstractCalendar getCalendar()
	{
		return c;
	}
	
	
}
