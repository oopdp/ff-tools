/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools.capital;
import java.time.ZonedDateTime;

/**
 *
 * @author salvix
 */
public class Transaction {
	
	
	/**
	 * 
	 */
	private double amount;
	
	/**
	 * 
	 */
	private ZonedDateTime zdt;
	
	/**
	 * 
	 * @param a amount
	 * @param z time of transaction
	 */
	public Transaction(double a, ZonedDateTime z)
	{
		amount = a;
		zdt = z;
	}
}
