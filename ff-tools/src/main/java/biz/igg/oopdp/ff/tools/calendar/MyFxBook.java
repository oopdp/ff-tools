/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools.calendar;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

/**
 *
 * @author salvix
 */
public class MyFxBook implements AbstractCalendar {
	
	
	public String response;
	
	public String cleanedResponse;
	
	private CalendarMap cm;
	
	public MyFxBook(String response)
	{
		this.response = response;
	}
	
	public CalendarMap getCalendarMap()
	{
		return cm;
	}
	
	
	public void makeCalendarMap() throws IOException
	{
		cleanResponse();
		parse(cleanedResponse);
	}
	
	public String cleanResponse()
	{
		cleanedResponse = cleanse(response);
		cleanedResponse = extractCalendar(cleanedResponse);
		cleanedResponse = fixSelfClosingTags(cleanedResponse);
		return cleanedResponse;
	}
	
	public void parse(String parse) throws IOException
	{
		Reader reader = new StringReader(parse);
		HTMLEditorKit.Parser parser = new ParserDelegator();
		MyFxBookHtmlParser p = new MyFxBookHtmlParser();
		cm = new CalendarMap();
		p.setCalendarMap(cm);
		parser.parse(reader, p, true);
		reader.close();
	}
	
	public String cleanse(String str)
	{
		str = str.replaceAll("\\n", "");
		str = str.replaceAll("\\r", "");
		Pattern pattern = Pattern.compile("<response error=\"false\" code=\"\" message=\"\"><!\\[CDATA\\[ (.+?)\\]\\]></response>");
		Matcher matcher = pattern.matcher(str);
		matcher.find();
		return matcher.group(1);
	}
	
	public String extractCalendar(String str)
	{
		String tTag = "<table .* id=\"calendar\".*>\\s*";
		Pattern pattern = Pattern.compile(tTag+"(.+?)\\s*</table>");
		Matcher matcher = pattern.matcher(str);
		boolean matched = matcher.find();
		if(matched){
			return matcher.group(0);
		}
		return "";
	}
	
	
	public String fixSelfClosingTags(String str)
	{
//		str = str.replaceAll("/>", ">");
		
		String copy = new String(str);
		Pattern pattern = Pattern.compile("<img src=\"http://www.myfxbook.com/images/transparent.png\" (.+?)/>");
		Matcher matcher = pattern.matcher(str);
//		matcher.find();
		int count = 0;
		String sm;
        while(matcher.find()) {
            count++;
			sm = str.substring(matcher.start(), matcher.end());
			Pattern spatt = Pattern.compile("class=\"(.+?)\"");
			Matcher smatch = spatt.matcher(sm);
			smatch.find();
			String r = smatch.group(1);
			String pad = String.format("%0" + (sm.length() - r.length()) + "d", 0).replace("0", " ");
			r += pad;
			copy = copy.replace(sm, r);
        }
		return copy;
	}
	
	public void parse() throws IOException
	{
//		parse(response);
	}
}
