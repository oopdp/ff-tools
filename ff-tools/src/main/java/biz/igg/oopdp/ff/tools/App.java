package biz.igg.oopdp.ff.tools;

import java.util.Calendar;
import biz.igg.oopdp.ff.tools.capital.SimpleInterest;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
		Ui.start(args);
    }
	
	public static String calculateSimpleInterest(String amount, String interest, String from, String to)
	{
		try {
			double c = Double.valueOf(amount);
			double i = Double.valueOf(interest);
			SimpleInterest s = new SimpleInterest(c, i);
			s.setStart(convertDate(from));
			s.setEnd(convertDate(to));
			return buildOutput(c, i, s.totalInterest(), s.dailyInterest(), s.numberOfDays());
		}
		catch(Exception e){
		
		}
		return "Sorry, data provided are not valid";
	}
	
	
	public static String buildOutput(double amount, double interestRate, double totalInterest, double dailyInterest, int nDays)
	{
		String dF = "%.2f";
		String s = "<html>"+String.format(dF, amount);
		s += "@"+String.format(dF, interestRate)+"%";
		s += " for "+nDays+" days <br>";
		s += String.format(dF, totalInterest);
		s += " with ";
		s += String.format(dF, dailyInterest);
		s += "/day";
		s += "</html>";
		return s;
	}
	public static Calendar convertDate(String date)
	{
		System.out.println(date);
		if(date.equals("today")){
			return Calendar.getInstance();
		}
		Calendar.Builder cb = new Calendar.Builder();
		String[] tokens = date.split("-");
		if(tokens.length == 3){
			int year = 0;
			int month = 0;
			int day = 0;
			year = Integer.valueOf(tokens[2].replaceFirst("^0+(?!$)", ""));
			month = Integer.valueOf(tokens[1].replaceFirst("^0+(?!$)", ""));
			day = Integer.valueOf(tokens[0].replaceFirst("^0+(?!$)", ""));
			cb.setDate(year, month, day);
		}
		return cb.build();
	}

}
