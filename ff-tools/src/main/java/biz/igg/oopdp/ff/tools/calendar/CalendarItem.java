/*
 * To change this license header, choose License Headers in Project Properties+
 * To change this template file, choose Tools | Templates
 * and open the template in the editor+
 */
package biz.igg.oopdp.ff.tools.calendar;

/**
 *
 * @author salvix
 */
public class CalendarItem {
	
	public String id;
	
	public String remoteId;
	
	public String date;
	
	public String event;
	
	public String description;
	
	public String impact;
	
	public String unit;
	
	public String country;
	
	public String previous;
	
	public String consensus;
	
	public String actual;
	
	public String releaser;
	
	public String url;
	
	@Override
	public String toString()
	{
		return "|"+date+"|"+country+"|"+event+"["+unit+"]|"+impact+"|"+previous+"|"+consensus+"|"+actual+"\n";
	}
}
