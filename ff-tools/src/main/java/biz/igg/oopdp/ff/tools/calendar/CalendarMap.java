/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools.calendar;

import java.util.HashMap;

/**
 *
 * @author salvix
 */
public class CalendarMap extends HashMap<String, CalendarItem> {
	
	
	@Override
	public String toString()
	{
		String parent = super.toString();
		return "KEY=|DATE|COUNTRY|EVENT|IMPACT|P|C|A\n"+parent;
	}
}
