/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools.calendar;

import java.io.IOException;

/**
 *
 * @author salvix
 */
public interface AbstractCalendar {
	
	public CalendarMap getCalendarMap();
	
	public void makeCalendarMap() throws IOException;
}
