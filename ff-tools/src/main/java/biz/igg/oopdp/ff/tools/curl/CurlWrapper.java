/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools.curl;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

/**
 *
 * @author salvix
 */
public class CurlWrapper {
	
	
	public static String get(String url) throws MalformedURLException, IOException
	{
		Curl c = new Curl();		
		c.setUrl(url);
		HttpURLConnection cn = c.get();
		String output = c.getOutput(cn);
		return output;
	}
	
	public static String get(String url, String proxyUrl, int proxyPort) throws MalformedURLException, IOException
	{
		Curl c = new Curl();		
		c.setUrl(url);
		c.setProxy(proxyUrl, proxyPort);
		HttpURLConnection cn = c.get();
		String output = c.getOutput(cn);
		return output;
	}
}
