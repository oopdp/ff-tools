/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools.calendar;

import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.management.Attribute;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTML.Tag;
import javax.swing.text.html.HTMLEditorKit;

/**
 *
 * @author salvix
 */
public class MyFxBookHtmlParser extends HTMLEditorKit.ParserCallback {
	
	private boolean debug = false;
	
	private boolean inTable = false;
	
	private CalendarMap cm;
	
	private String id = null;
	
	private int counter;
	

	private static final String SELFURL = "http://www.myfxbook.com";
	
	private String[] details;
	
	
	private void p(Object x)
	{
		System.out.println(x);
	}
	
	private void d(Object x)
	{
		if(debug) p(x);
	}
	
	public void setCalendarMap(CalendarMap cm)
	{
		this.cm = cm;
	}
	
	public CalendarMap getCalendarMap()
	{
		return this.cm;
	}
	
//	public void handleStartTag()
			
	private void printTagAndAttributes(Tag tag, MutableAttributeSet attrSet, int pos)
	{
		Enumeration attributeNames = attrSet.getAttributeNames();
		while(attributeNames.hasMoreElements()){
			Object attr = attributeNames.nextElement();
			String attrName = attr.toString();
//			d("Tag "+tag+" With Attributes => " +  attrName + " : " + attrSet.getAttribute(attr));
		}
	}
    public void handleStartTag(Tag tag, MutableAttributeSet attrSet, int pos) {
//		p("ss");
//		if(tag.toString().equals("img")){
//			p("IMAGE!!!!");
//		}
//		if(tag.equals(Tag.IMG)){
//				p("IMAGE!!!!");
////				printTagAndAttributes(tag, attrSet, pos);
//		}
		printTagAndAttributes(tag, attrSet, pos);
        if(!inTable){
			if(tag.equals(Tag.TABLE)){
				if(attrSet.containsAttribute(HTML.Attribute.ID, "calendar")){
					inTable = true;
				}
			}
		}
		else {
			if(tag.equals(Tag.TR)){
				if(attrSet.isDefined(HTML.Attribute.ID)){
					Object attr = attrSet.getAttribute(HTML.Attribute.ID);
					createNewEntry(attr.toString());
				}
			}
			if(tag.equals(Tag.TD)){
				if(hasIdMatchingPattern(attrSet, "previousTip.*")){
					Object attr = attrSet.getAttribute("unit");
					addDetailToEntry(attr.toString(), "unit");
					counter = 4;
				}
				if(hasIdMatchingPattern(attrSet, "concensus.*")){
					Object attr = attrSet.getAttribute("unit");
					counter = 5;
				}
				if(hasIdMatchingPattern(attrSet, "actualTip.*")){
					Object attr = attrSet.getAttribute("unit");
					counter = 6;
				}
			}
			if(tag.equals(Tag.SPAN)){
				if(hasIdMatchingPattern(attrSet, "calendarTip.*")){
					if(attrSet.isDefined(HTML.Attribute.CLASS)){
						Object attr = attrSet.getAttribute(HTML.Attribute.CLASS);
						addDetailToEntry(attr.toString(), "country");
					}
				}
			}
			if(tag.equals(Tag.A)){
				if(attrSet.isDefined(HTML.Attribute.HREF)){
					Object attr = attrSet.getAttribute(HTML.Attribute.HREF);
					if(findPattern(SELFURL+".*", attr.toString())){
						
					}
					else {
						if(findPattern("http://.*", attr.toString())){
							addDetailToEntry(attr.toString());
						}
					}
				}
			}
			
		}
        
    }
	
	
	
	private boolean hasAttributeValueMatchingPattern(MutableAttributeSet attrSet, HTML.Attribute attr, String pattern)
	{
		if(attrSet.isDefined(attr)){
			Object value = attrSet.getAttribute(attr);
			return findPattern(pattern, value.toString());
		}
		return false;
	}
	
	private boolean hasClassMatchingPattern(MutableAttributeSet attrSet, String pattern)
	{
		return hasAttributeValueMatchingPattern(attrSet, HTML.Attribute.CLASS, pattern);
	}
	
	private boolean hasIdMatchingPattern(MutableAttributeSet attrSet, String pattern)
	{
		return hasAttributeValueMatchingPattern(attrSet, HTML.Attribute.ID, pattern);
	}
	
	private boolean findPattern(String pattern, String searchString)
	{
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(searchString);
		return m.matches();			
	}
	
	public void createNewEntry(String id){
//		p(id);
		id = id.replaceAll("data", "");
		id = id.replaceAll("calRow", "");
		if(cm.containsKey(id)){
//			p("THIS IS NOT A NEW ENTRY HERE  "+this.id);
			counter = 7;
		}
		else {
			flushEntry();
			CalendarItem ci = new CalendarItem();
			ci.id = id;
			ci.remoteId = id;
			this.id = id;
			counter = -1;
			details = new String[50];
			cm.put(id, ci);
		}
	}
	
	public String cleanseString(String s)
	{
		String c = s.trim();
		c = c.replaceAll("\u00a0", "");
		return c;
	}
	
	public void flushEntry()
	{	if(id != null){
			d("FLUSHING:"+id);
			CalendarItem ci = cm.get(id);
			
			for(int i = 0; i <= counter; i++){
				d(i+":"+details[i]);
				setPropertyByIndex(ci, i, details[i]);
			}
		}
		else {
			d("CANT FLUSH YET!");
		}
	}
	
	public void addDetailToEntry(String detail, String scope)
	{
		counter++;
		if(debug){
			details[counter] = scope+":"+cleanseString(detail);
		}
		else {
			details[counter] = cleanseString(detail);
		}
	}
	
	public void addDetailToEntry(String detail)
	{
		addDetailToEntry(detail, "?");
	}
	
	
	
	private void setPropertyByIndex(CalendarItem ci, int index, String detail)
	{
//		|DATE|COUNTRY|EVENT|IMPACT|P|C|A0
		switch(index){
			case 0:
				ci.date = detail;
			break;
			case 1:
				ci.country = detail;
			break;
			case 2:
				ci.event = detail;
			break;
			case 3:
				ci.impact = detail;
			break;
			case 4:
				ci.unit = detail;
			break;
			case 5:
				ci.previous = detail;
			break;
			case 6:
				ci.consensus = detail;
			break;
			case 7:
				ci.actual = detail;
			break;
		}
	}
	
    public void handleText(char[] data, int pos) {
		if(inTable && id != null){
			filterIncomingText(new String(data));
		}
    }
	
	private void filterIncomingText(String detail)
	{
		String scope = "?";
		
		String [] blacklist = {"sprite.*sprite-countries.*", ".*sprite-tick inline"};
		String [] impacts = {"high", "medium", "low", "no"};
		
		for(String s: blacklist){
			if(detail.matches(s)){
				return;
			}
		}
		
		if(detail.matches("Link")){
			detail = String.format("%0" + 10 + "d", 0).replace("0", "-");
		}
		if(detail.matches("sprite.*impact")){
			scope = "impact";
			for(String s: impacts){
				if(detail.matches(".*"+s+".*")){
					detail = s.substring(0, 1).toUpperCase();
				}
			}
		}
		addDetailToEntry(detail, scope);
	}
	
     
    public void handleComment(char[] data, int pos) {
//        p("Comment Text => " + new String(data));
    }
 
    public void handleEndOfLineString(String data) {
        // This is invoked after the stream has been parsed, but before flush. 
        // eol will be one of \n, \r or \r\n, which ever is 
        // encountered the most in parsing the stream.
//        p("End of Line String => " + data);
    }
 
    public void handleEndTag(Tag tag, int pos) {
		
		if(tag.equals(Tag.TABLE) && inTable){
			flushEntry();
		}
		
    }
 
    public void handleError(String err, int pos) {
//        p("Error => " + err);
    }
}
