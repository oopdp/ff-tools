/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools.curl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URL;


/**
 *
 * @author salvix
 */
public class Curl {
	
	private String url;
	
	private Proxy proxy = null;
	
	private HttpURLConnection connection;
	
	private URL target;
	
	public void setUrl(String url) throws MalformedURLException
	{
		this.url = url;
		target = new URL(this.url);
	}
	
	public void setProxy(String url, int port)
	{
		SocketAddress addr = new InetSocketAddress(url, port);
		proxy = new Proxy(Proxy.Type.SOCKS, addr);
	}
	
	public HttpURLConnection post() throws IOException
	{
		HttpURLConnection conn = (HttpURLConnection) target.openConnection();
		conn.setReadTimeout(10000);
		conn.setConnectTimeout(15000);
		conn.setRequestMethod("POST");
		conn.setDoInput(true);
		conn.setDoOutput(true);
		connection = conn;
		return getConnection();
	}
	
	public HttpURLConnection get() throws IOException
	{
		HttpURLConnection conn;
		if(proxy == null){
			conn = (HttpURLConnection) target.openConnection();
		}
		else {
			conn = (HttpURLConnection) target.openConnection(proxy);
		}
		conn.setReadTimeout(10000);
		conn.setConnectTimeout(15000);
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		conn.setDoOutput(true);
		connection = conn;
		return getConnection();
	}
	
	public HttpURLConnection getConnection()
	{
		return connection;
	}
	
	public void writeParams(HttpURLConnection conn, KeyValueList params) throws IOException
	{
		OutputStream os = conn.getOutputStream();
		BufferedWriter writer = new BufferedWriter(
		new OutputStreamWriter(os, "UTF-8"));
		writer.write(params.getQuery());
		writer.flush();
		writer.close();
	}
	
	
	public String getOutput(HttpURLConnection conn) throws IOException
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = br.readLine()) != null) {
			sb.append(line);
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}
}
