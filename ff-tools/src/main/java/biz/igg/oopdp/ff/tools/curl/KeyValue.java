/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools.curl;

/**
 *
 * @author salvix
 */
public class KeyValue {
	
	public String key;
	public String value;
	
	public KeyValue(String key, String value)
	{
		this.key = key;
		this.value = value;
	}
}
