/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools.capital;

import java.util.Calendar;

/**
 *
 * @author salvix
 */
public class SimpleInterest {
	
	private double amount = 0;
	
	private double interest = 0;
	
	private Calendar start;
	
	private Calendar end;
	
	private static final int DAYS_IN_YEAR = 365;
	
	public SimpleInterest(double amount, double interest)
	{
		this.amount = amount;
		this.interest = interest;
		start = new Calendar.Builder().build();
		end = new Calendar.Builder().build();
	}
	
	public void setStart(Calendar start)
	{
		this.start = start;
	}
	
	public void setEnd(Calendar end)
	{
		this.end = end;
	}
	
	/**
	 *                (n-days)
	 * I = capital * ---------- * annual_int_rate (LIKE 8% ANNUM!)
	 *                   365    
	 * @return 
	 */
	public double totalInterest()
	{
		int nDays = numberOfDays();
		return interestFormula(nDays);
	}
	
	private double interestFormula(int nDays)
	{
		return (amount * nDays * interest) / (DAYS_IN_YEAR * 100);
	}
	
	public double dailyInterest()
	{
		return interestFormula(1);
	}
	
	/**
	 * 
	 * @return 
	 */
	public int numberOfDays()
	{
		return (int) ((end.getTimeInMillis() - start.getTimeInMillis()) / (1000 * 60 * 60 * 24));
	}
	
	
}
