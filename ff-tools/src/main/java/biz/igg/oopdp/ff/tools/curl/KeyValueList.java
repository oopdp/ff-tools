/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools.curl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;


/**
 *
 * @author salvix
 */
public class KeyValueList extends ArrayList<KeyValue> {
	
	
	public String getQuery() throws UnsupportedEncodingException
	{
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for (KeyValue pair : this)
		{
			if (first)
				first = false;
			else
				result.append("&");

			result.append(URLEncoder.encode(pair.key, "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(pair.value, "UTF-8"));
		}
		return result.toString();
	}
}
