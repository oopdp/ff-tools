/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools.calendar;

import biz.igg.oopdp.ff.tools.calendar.MyFxBook;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Scanner;
import junit.framework.TestCase;

import biz.igg.oopdp.ff.tools.curl.Curl;

/**
 *
 * @author salvix
 */
public class MyFxBookTest extends TestCase {
	
	public void testParse1()  throws Exception
	{
		String content = new Scanner(getFile("MyFxBook.xml")).useDelimiter("\\Z").next();
		MyFxBook fx = new MyFxBook(content);
		content = fx.cleanse(content);
		content = fx.extractCalendar(content);
		content = fx.fixSelfClosingTags(content);
		fx.parse(content);
	}
	
	public void testParse2()  throws Exception
	{
		String content = new Scanner(getFile("MyFxBook2.xml")).useDelimiter("\\Z").next();
		MyFxBook fx = new MyFxBook(content);
		content = fx.cleanse(content);
		content = fx.extractCalendar(content);
		content = fx.fixSelfClosingTags(content);
		fx.parse(content);
	}
	
	public void testParse3()  throws Exception
	{
		String content = new Scanner(getFile("MyFxBook3.xml")).useDelimiter("\\Z").next();
		MyFxBook fx = new MyFxBook(content);
		content = fx.cleanse(content);
		content = fx.extractCalendar(content);
		content = fx.fixSelfClosingTags(content);
		fx.parse(content);
	}
	
	public void testParse4()  throws Exception
	{
		String content = new Scanner(getFile("MyFxBook4.xml")).useDelimiter("\\Z").next();
		MyFxBook fx = new MyFxBook(content);
		content = fx.cleanse(content);
		content = fx.extractCalendar(content);
		content = fx.fixSelfClosingTags(content);
		fx.parse(content);
	}
	
	public void testParse5()  throws Exception
	{
		String content = new Scanner(getFile("MyFxBook5.xml")).useDelimiter("\\Z").next();
		MyFxBook fx = new MyFxBook(content);
		content = fx.cleanse(content);
		content = fx.extractCalendar(content);
		content = fx.fixSelfClosingTags(content);
		fx.parse(content);
	}
	
	
	public void testParse6()  throws Exception
	{
		String content = new Scanner(getFile("MyFxBook6.xml")).useDelimiter("\\Z").next();
		MyFxBook fx = new MyFxBook(content);
		content = fx.cleanse(content);
		content = fx.extractCalendar(content);
		content = fx.fixSelfClosingTags(content);
		fx.parse(content);
	}
	private File getFile(String fileName)
	{
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());
		return file;
	}
}
