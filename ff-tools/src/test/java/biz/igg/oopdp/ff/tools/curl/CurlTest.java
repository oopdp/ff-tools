/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools.curl;

import biz.igg.oopdp.ff.tools.curl.KeyValueList;
import biz.igg.oopdp.ff.tools.curl.KeyValue;
import java.io.IOException;
import java.net.HttpURLConnection;
import biz.igg.oopdp.ff.tools.curl.Curl;
import junit.framework.TestCase;
import biz.igg.oopdp.ff.tools.calendar.CalendarMap;
import biz.igg.oopdp.ff.tools.calendar.MyFxBook;

/**
 *
 * @author salvix
 */
public class CurlTest extends TestCase {
	
	/**
	 * 
	 * @return 
	 */
	public static KeyValueList calParams()
	{
		KeyValueList params = new KeyValueList();
		params.add(new KeyValue("min", ""));
		params.add(new KeyValue("start", ""));
		params.add(new KeyValue("end", ""));
		params.add(new KeyValue("filter", "0-1-2-3_PEI-CNY-JPY-CZK-MXN-CAD-ZAR-AUD-NZD-CLP-GBP-NOK-ISK-CHF-RUB-ANG-ARS-INR-EEK-IDR-TRY-ROL-SGD-QAR-HKD-COP-DKK-SEK-BRL-EUR-HUF-PLN-USD-KRW-KPW"));
		params.add(new KeyValue("show", "show"));
		params.add(new KeyValue("type", "cal"));
		params.add(new KeyValue("calPeriod", "8"));
		params.add(new KeyValue("rand", "0.23844808995311972"));
		return params;
	}
	
	/**
	 * 
	 * @return 
	 */
	public static KeyValueList params()
	{
		KeyValueList params = new KeyValueList();
		params.add(new KeyValue("firstParam", "1"));
		params.add(new KeyValue("secondParam", "2"));
		params.add(new KeyValue("thirdParam", "3"));
		return params;
	}
	
	/**
	 * 
	 * @param content
	 * @return
	 * @throws IOException 
	 */
	private CalendarMap parseOutput(String content) throws IOException
	{
		MyFxBook fx = new MyFxBook(content);
		content = fx.cleanse(content);
		content = fx.extractCalendar(content);
		content = fx.fixSelfClosingTags(content);
		fx.parse(content);
		CalendarMap cm = fx.getCalendarMap();
		return cm;
	}
	
	
	/**
	 * 
	 * @throws Exception 
	 *
	public void testCurl() throws Exception
    {
		Curl c = new Curl();		
//		c.setUrl("http://www.forexfactory.com/flex.php");
//		String tUrl = "http://posttestserver.com/post.php";
		String tUrl = "https://httpbin.org/post";
		c.setUrl(tUrl);
		HttpURLConnection cn = c.post();
		c.writeParams(cn, params());
		String output = c.getOutput(cn);
		String exp = "";
		assertEquals(exp, output);
    }
	
	/**
	 * 
	 * @throws Exception 
	 *
	public void testGetCurl()  throws Exception
	{
		String url = "http://www.myfxbook.com/calendarEmailMinAlert.xml?min=&start=2016-11-05%2000:00&end=2016-11-07%2000:00&filter=0-1-2-3_ANG-ARS-AUD-BRL-CAD-CHF-CLP-CNY-COP-CZK-DKK-EEK-EUR-GBP-HKD-HUF-IDR-INR-ISK-JPY-KPW-KRW-MXN-NOK-NZD-PEI-PLN-QAR-ROL-RUB-SEK-SGD-TRY-USD-ZAR&show=show&type=cal&bubble=true&calPeriod=8&rand=0.7639011092001802";
		String tUrl = "http://httpbin.org/";
		tUrl = url;
		Curl c = new Curl();		
		c.setUrl(tUrl);
		HttpURLConnection cn = c.get();
		String output = c.getOutput(cn);
		CalendarMap cm = parseOutput(output);
		System.out.println(cm);
	}
	
	/**
	 * 
	 * @throws Exception 
	 *
	public void testGetCurl2()  throws Exception
	{
		String url = "http://www.myfxbook.com/calendarEmailMinAlert.xml?min=&start=2016-11-12%2000:00&end=2016-11-14%2000:00&filter=0-1-2-3_PEI-CNY-JPY-CZK-MXN-CAD-ZAR-AUD-NZD-CLP-GBP-NOK-ISK-CHF-RUB-ANG-ARS-INR-EEK-IDR-TRY-ROL-SGD-QAR-HKD-COP-DKK-SEK-BRL-EUR-HUF-PLN-USD-KRW-KPW&show=show&type=cal&bubble=true&calPeriod=3&rand=0.23844808995311972";
		Curl c = new Curl();		
		c.setUrl(url);
		HttpURLConnection cn = c.get();
		String output = c.getOutput(cn);
		CalendarMap cm = parseOutput(output);
		System.out.println(cm);
	}
	
	
	/**
	 * 
	 * @throws Exception 
	 *
	public void testGetCurl3()  throws Exception
	{
		String exp = "http://www.myfxbook.com/calendarEmailMinAlert.xml?min=&start=2016-11-12%2000:00&end=2016-11-14%2000:00&filter=0-1-2-3_PEI-CNY-JPY-CZK-MXN-CAD-ZAR-AUD-NZD-CLP-GBP-NOK-ISK-CHF-RUB-ANG-ARS-INR-EEK-IDR-TRY-ROL-SGD-QAR-HKD-COP-DKK-SEK-BRL-EUR-HUF-PLN-USD-KRW-KPW&show=show&type=cal&bubble=true&calPeriod=3&rand=0.23844808995311972";
		String url = "http://www.myfxbook.com/calendarEmailMinAlert.xml";
		KeyValueList kv = calParams();
		String q = kv.getQuery();
//		assertEquals(exp, url+"?"+q);
		System.out.println(exp);
		System.out.println(url+"?"+q);
		Curl c = new Curl();		
		c.setUrl(url+"?"+q);
		HttpURLConnection cn = c.get();
		String output = c.getOutput(cn);
		CalendarMap cm = parseOutput(output);
		System.out.println(cm);
	}
	
	/**
	 * 
	 */
	private void a()
	{
		
	}
	
	public void testEmpty()
    {
        assertTrue( true );
    }
}
