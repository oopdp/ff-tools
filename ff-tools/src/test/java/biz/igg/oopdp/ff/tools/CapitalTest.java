/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biz.igg.oopdp.ff.tools;

import java.util.Calendar;
import java.util.Date;
import junit.framework.TestCase;
import biz.igg.oopdp.ff.tools.capital.SimpleInterest;

/**
 *
 * @author salvix
 */
public class CapitalTest extends TestCase {
	
	
	public static SimpleInterest build()
	{
		SimpleInterest s = new SimpleInterest(1000, 8);
		Calendar.Builder start = new Calendar.Builder();
		Calendar.Builder end = new Calendar.Builder();
		start.setDate(2016, 1, 1);
		end.setDate(2016, 1, 15);
		s.setStart(start.build());
		s.setEnd(end.build());
		return s;
	}
	
	 /**
     * Rigourous Test :-)
     */
    public void testSimpleInterest()
    {
        assertTrue( true );
		SimpleInterest s = build();
		int nDays = s.numberOfDays();
		assertEquals(14, nDays);
    }
	
	public void testSimpleInterest_totalInterest()
    {
        assertTrue( true );
		SimpleInterest s = build();
		double i = s.totalInterest();
		assertEquals(3.068, i, 0.001);
    }
	
	
	public void testSimpleInterest_dailyInterest()
    {
        assertTrue( true );
		SimpleInterest s = build();
		double i = s.dailyInterest();
		assertEquals(0.219, i, 0.001);
    }
}
